## Insert your Dockerfile directives here to build the proper Docker image
FROM ubuntu:latest
run apt-get update -y
run apt-get install -y python-pip python-dev build-essential
copy . /app
WORKDIR /app
run pip install -r requirements.txt
ENTRYPOINT ["python", "app.py"]

